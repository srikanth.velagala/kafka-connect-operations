# Introduction

This document describes Operations for Kafka connect framework.

## Prerequisites

Ansible '2.9.5'

## Overview

Kafka Connect operations framework supports add, delete and pause operations for the following connectors.

  1. HDFS2 Sink Connector

  2. HDFS2 Source Connector

## Installation

Install Ansible

```bash
sudo yum install python27
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install ansible
```

Make sure that connector names for pause, delete and add operations are mutually exclusive.
Flow of Connect operations is as follows:

1. Pause Operation

2. Delete Operation

3. Add Operation

## Build

run.sh. This script performs filecopy andexecutes ansible playbook
```bash
./run.sh dev
```


## Functionality

1. To add HDFS2 sink/source connector, add entires in hdfs2-sink.json and/or hdfs2-source.json respectively in configs/dev directory and configure the required fields in those templates.

2. To delete any connector, add entries to delete.json in configs/dev directory as follows
```bash
{ "name": "existingconnectorname_to_delete-1"},
{ "name": "existingconnectorname_to_delete-2"}
```
3. To pause any connector, add entries to pause.json in configs/dev directory.
```bash
{ "name": "existingconnectorname_to_pause-1"},
{ "name": "existingconnectorname_to_pause-2"}
```

## Configure New Connectors

Perform following steps to configure and add new connectors

1. Design the new connector j2 template and add to operations/roles/connect/templates directory

2. Design the new connector configurable file with required fields and add it to configs/dev directory

3. Add name of the file from step 2 to connect_configs variable in operations/roles/connect/defaults/main.yml
