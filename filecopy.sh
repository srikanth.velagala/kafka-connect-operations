#!/bin/sh

deployEnv=$1

cp ./configs/${deployEnv}/hdfs2-sink.json ./operations/roles/connect/files/hdfs2-sink.json
cp ./configs/${deployEnv}/hdfs2-source.json ./operations/roles/connect/files/hdfs2-source.json
cp ./configs/${deployEnv}/dbz-db2-source.json ./operations/roles/connect/files/dbz-db2-source.json
cp ./configs/${deployEnv}/delete.json ./operations/roles/connect/files/delete.json
cp ./configs/${deployEnv}/pause.json ./operations/roles/connect/files/pause.json       