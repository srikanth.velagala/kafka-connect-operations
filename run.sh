#!/bin/sh

if [ $# -eq 0 ] || [ "$1" = "-h" ]   ; then
    echo -e "Usage:  `basename $0` [-h]\thelp"
    echo -e "\t`basename $0` ENVIRONMENT \trun ENVIRONMENT with the specified values"    
    echo -e "\tENVIRONMENT\t\tdev|qa|staging|prod"    
    echo -e "\nExample: ./`basename $0` dev "    
    exit 0
fi


deployEnv=${1}

./filecopy.sh ${deployEnv}

cd operations
./operations.yml -i ../configs/${deployEnv}/hosts.yml  --private-key /home/ubuntu/.ssh/id_rsa




              
